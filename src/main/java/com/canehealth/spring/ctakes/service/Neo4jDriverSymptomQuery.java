package com.canehealth.spring.ctakes.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.Values;
import org.neo4j.driver.v1.util.Pair;
import org.springframework.stereotype.Component;



//import clinical.pipeline.fast.temp.RunPipeline;
@Component
public class Neo4jDriverSymptomQuery {

	Value dname, sname, bodysite = null, orate, params, param, soriginalword, boriginalword;
	List<String> initSymptom = new ArrayList<String>();
	List<String> sympPresent = new ArrayList<String>();
	List<String> sympAbsent = new ArrayList<String>();
	HashMap<String, String> present = new HashMap<String, String>();
	HashMap<String, String> absent = new HashMap<String, String>();
	
	
	
	int count = 0;
	static private final Logger LOGGER = Logger.getLogger("Neo4jPopulation");
	public Driver driver;
	public BufferedReader reader;
	public AnalysisEngine ae;
	public JCas jcs;
	HashMap<String, String> onholdsymptom=new HashMap<String, String>();;
	
	List<String> ignoreList = Arrays.asList("Coughing","Fever","Chest Pain","Headache");
	public Neo4jDriverSymptomQuery() throws IOException, AnalysisEngineProcessException, UIMAException {

		new Thread() {
			public void run() {
				driver = GraphDatabase.driver("bolt://localhost:11001", AuthTokens.basic("neo4j", "1"));

			}
		}.start();

		
		

		reader = new BufferedReader(new InputStreamReader(System.in));

		LOGGER.info("done");
	}
	
	public String assessment(String useranswer) {
		if(useranswer.equalsIgnoreCase("yes")) {
			
			HashMap.Entry<String,String> entry = onholdsymptom.entrySet().iterator().next();
			 String symp = entry.getKey();
			 String site = entry.getValue();
		    sympPresent.add(symp);
			present.putAll(onholdsymptom);
			onholdsymptom.remove(symp);
		}
		else {
			
			HashMap.Entry<String,String> entry = onholdsymptom.entrySet().iterator().next();
			 String symp = entry.getKey();
			 String site = entry.getValue();
			 
		    sympAbsent.add(symp);
			absent.putAll(onholdsymptom);
			onholdsymptom.remove(symp);
		}
		return tempFunction();
	}
	public String tempFunction() {
		String statement,sitestatement;
		String message="";
		String initialstatement = "MATCH (d:DiseaseDisorderMention)-[r]-(s:SignSymptomMention)"
				+ " WHERE  s.preferredtext in {present} and not  s.preferredtext in {absent} " + " return d,r " + " ORDER BY r.weightage DESC LIMIT 1";

		params = Values.parameters("absent", sympAbsent, "present", sympPresent);
		try (Session session = driver.session()) {
			try (Transaction tx = session.beginTransaction()) {
				StatementResult rs = tx.run(initialstatement, params);

				if (rs.hasNext() && count<7) {
					Record res = rs.next();

					List<Pair<String, Value>> values = res.fields();
					for (Pair<String, Value> nameValue : values) {
						if ("d".equals(nameValue.key())) { // you named your node "p"
							Value value = nameValue.value();
							dname = value.get("preferredtext");
						}
					}
					statement = "MATCH (d:DiseaseDisorderMention{preferredtext:{diseasename}})-[r]-(s:SignSymptomMention)"
							/*
							 * + " WHERE s.preferredtext in {present} and not s.preferredtext in {absent}"
							 */
							+ " WHERE r.weightage>1  and not s.preferredtext in {present} and  not s.preferredtext in {absent} "
							+ " return s,r " + " ORDER BY r.weightage DESC LIMIT 3";
					param = Values.parameters("diseasename", dname, "present", sympPresent, "absent", sympAbsent);

					StatementResult rs1 = tx.run(statement, param);
					if (rs1.hasNext()) {

						Record res1 = rs1.next();

						List<Pair<String, Value>> values1 = res1.fields();
						for (Pair<String, Value> nameValue : values1) {
							if ("s".equals(nameValue.key())) { // you named your node "p"
								Value value = nameValue.value();
								sname = value.get("preferredtext");
								soriginalword = value.get("originalword");
								
								sitestatement = "MATCH (d:DiseaseDisorderMention{preferredtext:{diseasename}})-[a]-(b:AnatomicalSiteMention)-[a1]-(s:SignSymptomMention{preferredtext:{symptom}})"
										+ "  return b, count(a) as c, count(a1) as c1";
								Value siteparam = Values.parameters("diseasename", dname, "symptom", sname);
								StatementResult site = tx.run(sitestatement, siteparam);
							

								if (site.hasNext()) {
									Record sites = site.next();

									List<Pair<String, Value>> sitesvalues = sites.fields();
									for (Pair<String, Value> nameValue1 : sitesvalues) {
										if ("c".equals(nameValue1.key())) {
											Value value1 = nameValue1.value();

											if (value1.asInt() > 1) {
												System.out.println("two sutes fount");
											}
										}
										if ("b".equals(nameValue1.key())) { // you named your node "p"
											Value value1 = nameValue1.value();
											if (bodysite != null && bodysite.toString()
													.equalsIgnoreCase(value1.get("preferredtext").toString()))
												{bodysite=null;
											continue;}
											bodysite = value1.get("preferredtext");
											boriginalword = value1.get("originalword");

										}
									}
								}

							}
						}

						// Scanner reader = new Scanner(System.in); // Reading from System.in

						// if(bodysite!=null)
						// assesment = takeUserInput("Are you feeling " + soriginalword + "(" + sname +
						// ") in "+bodysite+" ? yes/no:");
						// else
						boolean sitecheck=true;
						if (bodysite != null) {
							 if(!(ignoreList.contains(soriginalword.toString())) && !(sname.toString().toLowerCase().contains(bodysite.toString().toLowerCase()))) {
								 
							 
						    sitecheck=false;
							message = "Are you feeling symptom of" + soriginalword.toString() + "(" + sname + ") in "
									+ bodysite.toString() + "? yes/no:";
							onholdsymptom.put(sname.toString(), bodysite.toString());
							 }
						} else {
							message = "Are you feeling symptom of" + soriginalword.toString() + "(" + sname
									+ ")? yes/no:";
							onholdsymptom.put(sname.toString(),"");
						    }
						String assesment = /*takeUserInput(message)*/"";
						count++;
                        
						return message;
						
						/*if (assesment.equalsIgnoreCase("yes")) {

							sympPresent.add(sname.toString());

							if (bodysite != null && sitecheck)
								present.put(sname.toString(), bodysite.toString());

						} else if (assesment.equalsIgnoreCase("no")) {
							
							if (bodysite != null && sitecheck)
								absent.put(sname.toString(), bodysite.toString());
							else
								sympAbsent.add(sname.toString());
							break;
						} else {

							LOGGER.info("Invalid Input");
						
						}*/

					}
					
					}
				}
		}
		
		
		return message;
	}

	private int tempFunction2() throws IOException, AnalysisEngineProcessException {
		String message;
		String symptom = takeUserInput("Enter a Symptom: ");
		
		String statement;
		String sitestatement = "MATCH (d:DiseaseDisorderMention)-[a]-(b:AnatomicalSiteMention)-[a1]-(s:SignSymptomMention{preferredtext:{symptom}})"
				+ "  return b";
		Value siteparam = Values.parameters("symptom", symptom);

		String initialstatement = "MATCH (d:DiseaseDisorderMention)-[r]-(s:SignSymptomMention)"
				+ " WHERE  s.preferredtext in {present}  " + " return d,r " + " ORDER BY r.weightage";

		params = Values.parameters("absent", sympAbsent, "present", sympPresent);

		try (Session session = driver.session()) {
			try (Transaction tx = session.beginTransaction()) {
				StatementResult rs = tx.run(initialstatement, params);

				while (rs.hasNext() && count<7) {

					Record res = rs.next();

					List<Pair<String, Value>> values = res.fields();
					for (Pair<String, Value> nameValue : values) {
						if ("d".equals(nameValue.key())) { // you named your node "p"
							Value value = nameValue.value();
							dname = value.get("preferredtext");
							// Value sorate = value.get("occurane_rate");
						}
					}

					statement = "MATCH (d:DiseaseDisorderMention{preferredtext:{diseasename}})-[r]-(s:SignSymptomMention)"
							/*
							 * + " WHERE s.preferredtext in {present} and not s.preferredtext in {absent}"
							 */
							+ " WHERE r.weightage>1  and not s.preferredtext in {present} and  not s.preferredtext in {absent} "
							+ " return s,r " + " ORDER BY r.weightage DESC LIMIT 3";
					param = Values.parameters("diseasename", dname, "present", sympPresent, "absent", sympAbsent);

					StatementResult rs1 = tx.run(statement, param);

					while (rs1.hasNext()) {

						Record res1 = rs1.next();

						List<Pair<String, Value>> values1 = res1.fields();
						for (Pair<String, Value> nameValue : values1) {
							if ("s".equals(nameValue.key())) { // you named your node "p"
								Value value = nameValue.value();
								sname = value.get("preferredtext");
								soriginalword = value.get("originalword");
								// Value sorate = value.get("occurane_rate");
								sitestatement = "MATCH (d:DiseaseDisorderMention{preferredtext:{diseasename}})-[a]-(b:AnatomicalSiteMention)-[a1]-(s:SignSymptomMention{preferredtext:{symptom}})"
										+ "  return b, count(a) as c, count(a1) as c1";
								siteparam = Values.parameters("diseasename", dname, "symptom", sname);
								StatementResult site = tx.run(sitestatement, siteparam);
							

								if (site.hasNext()) {
									Record sites = site.next();

									List<Pair<String, Value>> sitesvalues = sites.fields();
									for (Pair<String, Value> nameValue1 : sitesvalues) {
										if ("c".equals(nameValue1.key())) {
											Value value1 = nameValue1.value();

											if (value1.asInt() > 1) {
												System.out.println("two sutes fount");
											}
										}
										if ("b".equals(nameValue1.key())) { // you named your node "p"
											Value value1 = nameValue1.value();
											if (bodysite != null && bodysite.toString()
													.equalsIgnoreCase(value1.get("preferredtext").toString()))
												{bodysite=null;
											continue;}
											bodysite = value1.get("preferredtext");
											boriginalword = value1.get("originalword");

										}
									}
								}

							}
						}

						// Scanner reader = new Scanner(System.in); // Reading from System.in

						// if(bodysite!=null)
						// assesment = takeUserInput("Are you feeling " + soriginalword + "(" + sname +
						// ") in "+bodysite+" ? yes/no:");
						// else
						boolean sitecheck=true;
						if (bodysite != null && !(ignoreList.contains(soriginalword.toString())) && !(soriginalword.toString().toLowerCase().contains(bodysite.toString().toLowerCase()))) {
						    sitecheck=false;
							message = "Are you feeling symptom of" + soriginalword.toString() + "(" + sname + ") in "
									+ bodysite.toString() + "? yes/no:";

						} else
							message = "Are you feeling symptom of" + soriginalword.toString() + "(" + sname
									+ ")? yes/no:";

						String assesment = takeUserInput(message);
						count++;

						if (assesment.equalsIgnoreCase("yes")) {

							sympPresent.add(sname.toString());

							if (bodysite != null && sitecheck)
								present.put(sname.toString(), bodysite.toString());

						} else if (assesment.equalsIgnoreCase("no")) {
							sympAbsent.add(sname.toString());
							break;
						} else {

							LOGGER.info("Invalid Input");
							return 0;
						}

					}
					initialstatement = "MATCH (d:DiseaseDisorderMention)-[r]-(s:SignSymptomMention)"
							+ " WHERE not s.preferredtext in {absent}  " + " return d,r " + " ORDER BY r.weightage";

				}

				ArrayList<String> sites = new ArrayList<String>(present.keySet());
				ArrayList<String> anatomicalsitesymtom = new ArrayList<String>(present.values());
				String finalstatement = "MATCH (b:AnatomicalSiteMention)-[a1]-(d:DiseaseDisorderMention)-[r]-(s:SignSymptomMention)"

						+ " WHERE not s.preferredtext in {absent} and  b.preferredtext in {sites} and s.preferredtext in {anatomicalsitesymtom} "
						+ " return d,r " + " ORDER BY r.weightage";

				params = Values.parameters("absent", sympAbsent, "sites", sites, "anatomicalsitesymtom",
						anatomicalsitesymtom);

				StatementResult finalr = tx.run(finalstatement, params);
				System.out.println("Diagnosed");
				while (finalr.hasNext()) {

					Record res = finalr.next();

					List<Pair<String, Value>> values = res.fields();
					for (Pair<String, Value> nameValue : values) {
						if ("d".equals(nameValue.key())) { // you named your node "p"
							Value value = nameValue.value();
							dname = value.get("preferredtext");
							// Value sorate = value.get("occurane_rate");

							System.out.println(dname.toString());

						}
					}
				}
			
			tx.success();
		} // tx.close();
	}driver.close();

	return 0;

	}

	// ______________________________________________________________

	private String takeUserInput(String msg) throws IOException {
		String str = null;

		System.out.println(msg);
		str = reader.readLine();
		return str;
	}


	public static void main(String[] args) throws IOException, AnalysisEngineProcessException, UIMAException {

		Neo4jDriverSymptomQuery obj = new Neo4jDriverSymptomQuery();
		// String str=obj.takeUserInput("some");
		// obj.NLP("i am testing if i have a headache");
		obj.tempFunction2();
		System.out.print("end");
	}

}
